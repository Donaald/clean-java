package co.com.parqueadero.modelos;

import java.util.Objects;

public class MotoData extends VehiculoData {

    private String cilindraje;


    public MotoData(String id, String placa, String cilindraje) {
        super(id, placa);
        this.cilindraje = cilindraje;
    }

    public String getCilindraje() {
        return cilindraje;
    }

    public void setCilindraje(String cilindraje) {
        this.cilindraje = cilindraje;
    }

    @Override
    public String toString() {
        return "MotoData{" +
                "cilindraje='" + cilindraje + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MotoData)) return false;
        if (!super.equals(o)) return false;
        MotoData motoData = (MotoData) o;
        return Objects.equals(getCilindraje(), motoData.getCilindraje());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getCilindraje());
    }
}
