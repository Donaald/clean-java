package co.com.parqueadero.Convertidores;

import co.com.parqueadero.dominio.entidades.Carro;
import co.com.parqueadero.dominio.entidades.Moto;
import co.com.parqueadero.enums.Tipos;
import co.com.parqueadero.modelos.RegistroData;

import java.util.Date;

public final class RegistroConvertidor {


    public static <T> RegistroData registroConvertidor(T vehiculo) {
        RegistroData registroData = new RegistroData();
        registroData.setFechaEntrada(new Date());
        registroData.setRegisto(vehiculo);

        if (vehiculo instanceof Carro) {
            registroData.setTipo(Tipos.CARRO);
        }

        if (vehiculo instanceof Moto) {
            registroData.setTipo(Tipos.MOTO);
        }

        return registroData;

    }

    public static Carro registroCarro(RegistroData<Carro> registroData) {
        return new Carro(registroData.getRegisto().getId(), registroData.getRegisto().getPlaca());
    }


}
