package co.com.parqueadero.dominio.entidades;

import java.util.Objects;

public class Moto extends Vehiculo {

    private static Integer PRECIO_DIA = 4000;
    private static Integer PRECIO_HORA = 500;

    private String cilindraje;

    public Moto(String id, String placa, String cilindraje) {
        super(id, placa);
        this.cilindraje = cilindraje;
    }

    public static Integer getPrecioDia() {
        return PRECIO_DIA;
    }

    public static void setPrecioDia(Integer precioDia) {
        PRECIO_DIA = precioDia;
    }

    public static Integer getPrecioHora() {
        return PRECIO_HORA;
    }

    public static void setPrecioHora(Integer precioHora) {
        PRECIO_HORA = precioHora;
    }

    public String getCilindraje() {
        return cilindraje;
    }

    public void setCilindraje(String cilindraje) {
        this.cilindraje = cilindraje;
    }

    @Override
    public String toString() {
        return "Moto{" +
                "cilindraje='" + cilindraje + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Moto)) return false;
        Moto moto = (Moto) o;
        return Objects.equals(getCilindraje(), moto.getCilindraje());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCilindraje());
    }
}
