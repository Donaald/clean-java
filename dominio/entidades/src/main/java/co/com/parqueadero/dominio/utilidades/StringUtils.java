package co.com.parqueadero.dominio.utilidades;

import java.util.Objects;

public final class StringUtils {


    public static Boolean validarStringVacio(String... textos) {
        for (String valor : textos) {
            if (valor.equals("") || Objects.isNull(valor)) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

}
