package co.com.parqueadero.Convertidores;

import co.com.parqueadero.dominio.entidades.Carro;
import co.com.parqueadero.modelos.CarroData;

public final class CarroConvertidor {


    public static Carro carroDataCarroDominio(CarroData carroData) {
        return new Carro(carroData.getId(), carroData.getPlaca());

    }


}
