package co.com.parqueadero.Vehiculo;


import co.com.parqueadero.Vehiculo.DTO.CarroDTO;
import co.com.parqueadero.casosDeUso.puertaDeEnlace.RegistrarCarroUseCase;
import co.com.parqueadero.dominio.entidades.Carro;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(path = "/vehiculo", produces = "application/json")
public class Vehiculo {

    private final RegistrarCarroUseCase registrarCarroUseCase;


    public Vehiculo(RegistrarCarroUseCase registrarCarroUseCase) {
        this.registrarCarroUseCase = registrarCarroUseCase;
    }

    @PostMapping
    public Mono<Carro> consultarVehiculo(@RequestBody CarroDTO carro) {
        return Mono.just(carro)
                .map(carroDTO -> new Carro(carroDTO.getId(), carroDTO.getPlaca()))
                .flatMap(registrarCarroUseCase::registrarCarro);
    }


}
