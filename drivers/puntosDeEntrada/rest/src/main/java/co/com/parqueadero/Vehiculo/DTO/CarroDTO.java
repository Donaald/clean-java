package co.com.parqueadero.Vehiculo.DTO;


import java.util.Objects;

public class CarroDTO {

    private String id;
    private String placa;

    public CarroDTO(String id, String placa) {
        this.id = id;
        this.placa = placa;
    }

    public CarroDTO() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    @Override
    public String toString() {
        return "CarroDTO{" +
                "id='" + id + '\'' +
                ", placa='" + placa + '\'' +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CarroDTO)) return false;
        CarroDTO carroDTO = (CarroDTO) o;
        return Objects.equals(getId(), carroDTO.getId()) &&
                Objects.equals(getPlaca(), carroDTO.getPlaca());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getPlaca());
    }
}
