package co.com.parqueadero.modelos;

import java.util.Objects;

public class VehiculoData {

    private String id;
    private String placa;


    public VehiculoData(String id, String placa) {
        this.id = id;
        this.placa = placa;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    @Override
    public String toString() {
        return "VehiculoData{" +
                "id='" + id + '\'' +
                ", placa='" + placa + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof VehiculoData)) return false;
        VehiculoData that = (VehiculoData) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getPlaca(), that.getPlaca());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getPlaca());
    }
}
