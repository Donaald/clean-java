package co.com.parqueadero.casosDeUso;

import co.com.parqueadero.dominio.entidades.Carro;
import co.com.parqueadero.dominio.puertaDeEnlace.RegistrarCarro;
import co.com.parqueadero.dominio.puertaDeEnlace.ValidarExistenciaCarro;
import reactor.core.publisher.Mono;

public class RegistrarCarroUseCase implements co.com.parqueadero.casosDeUso.puertaDeEnlace.RegistrarCarroUseCase {

    private final ValidarExistenciaCarro validarExistenciaCarro;
    private final RegistrarCarro registrarCarro;


    public RegistrarCarroUseCase(ValidarExistenciaCarro validarExistenciaCarro, RegistrarCarro registrarCarro) {
        this.validarExistenciaCarro = validarExistenciaCarro;
        this.registrarCarro = registrarCarro;
    }

    @Override
    public Mono<Carro> registrarCarro(Carro carro) {
        return this.registrarCarro.registrarCarro(carro);
    }


}
