package co.com.parqueadero.dominio.entidades;

import java.util.Objects;

public class Vehiculo {

    private String id;
    private String placa;


    public Vehiculo(String id, String placa) {
        this.id = id;
        this.placa = placa;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }


    @Override
    public String toString() {
        return "Vehiculo{" +
                "id='" + id + '\'' +
                ", placa='" + placa + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vehiculo)) return false;
        Vehiculo vehiculo = (Vehiculo) o;
        return Objects.equals(getId(), vehiculo.getId()) &&
                Objects.equals(getPlaca(), vehiculo.getPlaca());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getPlaca());
    }
}
