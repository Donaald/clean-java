package co.com.parqueadero.dominio.puertaDeEnlace;


import co.com.parqueadero.dominio.entidades.Carro;
import reactor.core.publisher.Mono;

public interface RegistrarCarro {

    Mono<Carro> registrarCarro(Carro carro);


}
