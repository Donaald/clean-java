package co.com.parqueadero.casosDeUso.puertaDeEnlace;


import co.com.parqueadero.dominio.entidades.Carro;
import reactor.core.publisher.Mono;

public interface RegistrarCarroUseCase {

    Mono<Carro> registrarCarro(Carro carro);

}
