package co.com.parqueadero.configuracion;


import co.com.parqueadero.dominio.puertaDeEnlace.RegistrarCarro;
import co.com.parqueadero.dominio.puertaDeEnlace.ValidarExistenciaCarro;
import co.com.parqueadero.implementacion.RegistrarData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.ReactiveMongoOperations;


@Configuration
public class configuracion {


    private ValidarExistenciaCarro validarExistenciaCarro;

    @Autowired
    private ReactiveMongoOperations reactiveMongoOperations;

    @Autowired
    private RegistrarCarro registrarCarro;


    @Bean
    public co.com.parqueadero.casosDeUso.RegistrarCarroUseCase registrarCarroUseCase() {
        return new co.com.parqueadero.casosDeUso.RegistrarCarroUseCase(this.validarExistenciaCarro, this.registrarCarro);
    }


    @Bean
    public RegistrarCarro registrarCarroData() {
        return new RegistrarData(reactiveMongoOperations);
    }


}
