package co.com.parqueadero.dominio.puertaDeEnlace;

import reactor.core.publisher.Mono;

public interface ValidarExistenciaCarro {

    public Mono<Boolean> validarExistenciaCarro(String id);

}
