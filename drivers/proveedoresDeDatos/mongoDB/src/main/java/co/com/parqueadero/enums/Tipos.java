package co.com.parqueadero.enums;

public enum Tipos {
    MOTO("Moto"),
    CARRO("Carro");

    private final String tipo;

    Tipos(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }
}
