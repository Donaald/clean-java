package co.com.parqueadero.implementacion;

import co.com.parqueadero.Convertidores.RegistroConvertidor;
import co.com.parqueadero.dominio.entidades.Carro;
import co.com.parqueadero.dominio.puertaDeEnlace.RegistrarCarro;
import org.springframework.data.mongodb.core.ReactiveMongoOperations;
import reactor.core.publisher.Mono;

public class RegistrarData implements RegistrarCarro {

    private final ReactiveMongoOperations reactiveMongoOperations;

    public RegistrarData(ReactiveMongoOperations reactiveMongoOperations) {
        this.reactiveMongoOperations = reactiveMongoOperations;
    }

    @Override
    public Mono<Carro> registrarCarro(Carro carro) {
        return Mono.just(carro)
                .map(RegistroConvertidor::registroConvertidor)
                .flatMap(reactiveMongoOperations::save)
                .map(RegistroConvertidor::registroCarro);
    }

}
