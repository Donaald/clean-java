package co.com.parqueadero.modelos;


import co.com.parqueadero.enums.Tipos;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.Objects;

@Document(collection = "Registro")
public class RegistroData<T> {

    @Id
    private String id;
    private T registo;
    private Tipos tipo;
    private Date fechaEntrada;
    private Date fechaSalida;


    public RegistroData(T registo, Tipos tipo, Date fechaEntrada, Date fechaSalida) {
        this.registo = registo;
        this.tipo = tipo;
        this.fechaEntrada = fechaEntrada;
        this.fechaSalida = fechaSalida;
    }

    public RegistroData() {
    }

    public T getRegisto() {
        return registo;
    }

    public void setRegisto(T registo) {
        this.registo = registo;
    }

    public Tipos getTipo() {
        return tipo;
    }

    public void setTipo(Tipos tipo) {
        this.tipo = tipo;
    }

    public Date getFechaEntrada() {
        return fechaEntrada;
    }

    public void setFechaEntrada(Date fechaEntrada) {
        this.fechaEntrada = fechaEntrada;
    }

    public Date getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(Date fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "RegistroData{" +
                "id='" + id + '\'' +
                ", registo=" + registo +
                ", tipo=" + tipo +
                ", fechaEntrada=" + fechaEntrada +
                ", fechaSalida=" + fechaSalida +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RegistroData)) return false;
        RegistroData<?> that = (RegistroData<?>) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getRegisto(), that.getRegisto()) &&
                getTipo() == that.getTipo() &&
                Objects.equals(getFechaEntrada(), that.getFechaEntrada()) &&
                Objects.equals(getFechaSalida(), that.getFechaSalida());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getRegisto(), getTipo(), getFechaEntrada(), getFechaSalida());
    }
}
